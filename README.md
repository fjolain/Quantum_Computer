# Quantum Computer

This program simulate a quantic computer logic.
You can add qubits in your computer and perform somes gates on them.

To simplify use, program comes with this own small language to use it wihtout knowing python

## How to use
### By promter
execute directly `./computer` to launch a prompter. Then a help is available by tapping `>>> help`

### By an external program
Of course, all commands available in the prompter can be wroten in a external file. Then execute `./computer file` to start it.

### As a python library
Program is wroten in python, you can use directly computer.py as a library.

## Commands available
Currently computer can run these commands :

* qubit       : add a qubit in the computer
* clear       : delete the buffer qubit's names and set at zero the global state
* function    : add a function in the computer which can be used by Uf and oracle gates
* evaluate    : eval a function at a specific number 
* hadamard    : perform hadamard gate 
* pi8         : perform T or S(Pi/8) gate
* R           : Perform R = [1 0 ; 0 exp(2\*i\*Pi/2^k) ]
* X, Y, Z     : Perform Pauli matrix
* cnot        : perform C-NOT gate
* uf          : perform Uf gate with available function
* oracle      : perform oracle gate with available function
* s0          : perfrom Shift phase zero gate
* print       : print global state (not realistic function. Just to debug program)
* measure     : measure state by quantum approch (probabilistic and degeneracy)
* repeat      : repeat a bloc code many time

## Example
Some example are wroten in samples directory. You have : grover algorithm and deutsh algorithm
