#! /usr/bin/python3
# -*- coding: utf-8 -*-

import numpy as np
import math
import importlib
# clean and import function.py
f = open('function.py', 'w')
f.write("from math import * \n \n")
f.close()
import function

class Computer :
    """ This class contains multi qubits and can perform some gates on these qubits"""

    def __init__(self) :
        """ initialize the computer """
        self.clear()

        # tab containing function's name
        self.functions = []


    def clear(self) :
        """ Remove names qubits and entangled state. But NOT functions """
        self.state = np.ones(1, dtype=complex)
        self.nbrQubit = 0
        self.names = {}
        

    def addQubit(self, name, state) :
        """ add a qubit in the global entangled state 
            - name : qubits's name like 'x' or 'y'
            - state : 2 complex [a, b] <==> a|0> + b|1> .Must be normalized
        """
        
        # Check normalized
        norm = pow(state[0], 2) + pow(state[1], 2)
        if abs(norm-1) > 0.00001 :
            return "Qubit's state is not normalized"
        
        # Give an indice to the new qubit name
        if self.names == {} :
            self.names[name] = 0
        else :
            self.names[name] =  max(self.names.values())+1

        # Build the new state vector such as
        # [a b c d]+[x y] --> [ax ay bx by cx cy dx dy]
        new_state = np.zeros(len(self.state)*2, dtype=complex)
        for i in range(0, len(self.state)) :
            new_state[2*i]   = self.state[i]*state[0]
            new_state[2*i+1] = self.state[i]*state[1]

        # Update computer variables
        self.state = new_state
        self.nbrQubit += 1


    def __str__(self) :
        """ Print the current entangled state """
        s = ""
        for i in range(0, len(self.state)) :
            s += "{1}>  : {0}\n".format(self.state[i], i)
        return s

    def singleQubitOperator(self, n, a, b, c, d) :
        """ Compute a qubit or qubit tab by a generic gate.
            gate must be described by G = [a b; c d] matrix
            - n : qubit's name or list qubit's names
        """
        # Check if n is a list
        if type(n) == list :
            for qubit in n :
                tmp = self.singleQubitOperator(qubit, a, b, c, d)
                if tmp != None :
                    return tmp
            return None


        # Check arguments type
        if not n in self.names.keys():
            return "Qubit's name not found"

        new_state = np.zeros(len(self.state), dtype=complex)

        for i in range(0, len(self.state)) :
            binary = self.dec2bin(i)


            if binary[ self.names[n] ] == 0 :
                new_state[i] += a*self.state[i]
                binary[ self.names[n] ] = 1
                new_state[ self.bin2dec(binary) ] += c*self.state[i]

            elif binary[ self.names[n] ] == 1 :
                new_state[i] += d*self.state[i]
                binary[ self.names[n] ] = 0
                new_state[ self.bin2dec(binary) ] += b*self.state[i]

        self.state = new_state


    def hadamard(self, n) :
        """ Compute a hadamard gate on n qubit.
            - n : qubit's name or list qubit's names
        """
        return self.singleQubitOperator(n, 1/math.sqrt(2), 1/math.sqrt(2), 1/math.sqrt(2), -1/math.sqrt(2))

    def pi8(self, n) :
        """ Compute a S(Pi/8) gate on a qubit
            - n : qubit's name or list qubit's names
        """
        return self.singleQubitOperator(n, 1, 0, 0, complex(math.cos(math.pi/4), math.sin(math.pi/4)))

    def R(self, n, a) :
        """ Compute R = [exp(ia) 0, 0 exp(-ia)] gate
            - n : qubit's name or list qubit's names
        """
        return self.singleQubitOperator(n, complex(math.cos(a), math.sin(a)), 0, 0, complex(math.cos(-a), math.sin(-a)))

    def X(self, n) :
        """ Compute X pauli matrix
            - n : qubit's name or list qubit's names
        """
        return self.singleQubitOperator(n, 0, 1, 1, 0)

    def Y(self, n) :
        """ Compute Y pauli matrix
            - n : qubit's name or list qubit's names
        """
        return self.singleQubitOperator(n, -1j, 1j, 0)

    def Z(self, n) :
        """ Compute Z pauli matrix
            - n : qubit's name or list qubit's names
        """
        return self.singleQubitOperator(n, 1, 0, 0, -1)

    def cnot(self, n1, n2 ) :
        """ Compute a C-NOT gate on two qubit
            |n1, n2> ==> |n1, n1 xor n2>
        """

        # Check if arguments are list
        if type(n1) == list and type(n2) == list :
            # check if the tab have the same length
            if len(n1) != len(n2) :
                return "Tabs haven't the same length"
            for i in range(0, len(n1)) :
                tmp = self.cnot(n1[i], n2[i])
                if tmp != None :
                    return tmp
        return None

        # Check arguments type
        if (not n1 in self.names.keys()) or (not n2 in self.names.keys()) :
            return "Qubit's name not found"

        new_state = np.zeros(len(self.state), dtype=complex)

        for i in range(0, len(self.state)) :
            binary = self.dec2bin(i)

            binary[ self.names[n2] ] = (binary[ self.names[n2] ] + binary[ self.names[n1] ]) %2

            new_state[ self.bin2dec(binary) ] = self.state[i]

        self.state = new_state


    def uf(self, n1, n2, f) :
        """ Compute a Uf gate with f the function's name recorded by Computer.addFunction
            |n1, n2> ==> |n1, n2 xor f(n1)>
        """

        # Check if arguments are list
        if type(n1) == list and type(n2) == list :
            # check if the tab have the same length
            if len(n1) != len(n2) :
                return "Tabs haven't the same length"
            for i in range(0, len(n1)) :
                tmp = self.uf(n1[i], n2[i])
                if tmp != None :
                    return tmp
        return None

        # Check arguments type
        if (not n1 in self.names.keys()) or (not n2 in self.names.keys()) :
            return "Qubit's name not found"
        if not f in self.functions :
            return "Function's name not found"


        new_state = np.zeros(len(self.state), dtype=complex)

        for i in range(0, len(self.state)) :
            binary = self.dec2bin(i)

            binary[ self.names[n2] ] = (binary[ self.names[n2] ] 
                                     + self.evalFunction(f, binary[ self.names[n1] ])) %2
            new_state[ self.bin2dec(binary) ] += self.state[i]
        self.state = new_state


    def oracle(self, tab, n, f) :
        """ Perform an oracle function between a qubit tab and a qubit.
            It's a kind of uf() method with the single qubit as result
        """
        # Check arugments
        for q in tab :
            if not q in self.names.keys() :
                return "Qubit's name not found"
        if not n in self.names.keys() :
            return "Qubit's name not found"
        if not f in self.functions :
            return "Function's name not found"


        new_state = np.zeros(len(self.state), dtype=complex)

        # Extract qubit tab indice from the global englanted state
        indices_extract = []
        for q in tab :
            indices_extract.append(self.names[q])

        # Add single qubit at the end
        indices_extract.append(self.names[n])

        for i in range(0, len(self.state)) :
            binary = self.dec2bin(i)

            # Extract qubit in tab and single qubit from the global englanted state
            binary_extract = []
            for j in indices_extract :
                binary_extract.append(binary[j])


            # Perform oracle
            binary_extract[-1] = (binary_extract[-1] + self.evalFunction(f, self.bin2dec(binary_extract[0:-1])) )%2

            # Rebuild global binary
            for j in range(0, len(indices_extract)) :
                binary[indices_extract[j]] = binary_extract[j]

            # Change state
            new_state[ self.bin2dec(binary) ] = self.state[i]

        self.state = new_state


    def S0(self, tab) :
        """ Multiplie any state by -1 except |0> state """
        # Check arugments
        for q in tab :
            if not q in self.names.keys() :
                return "Qubit's name not found"

        # Extract qubit tab indice from the global englanted state
        indices_extract = []
        for q in tab :
            indices_extract.append(self.names[q])

        for i in range(0, len(self.state)) :
            binary = self.dec2bin(i)

            # Extract qubit tab from the global englanted state
            binary_extract = []
            for j in indices_extract :
                binary_extract.append(binary[j])
            if self.bin2dec(binary_extract) != 0 :
                self.state[i] = -self.state[i]


    def swap(self) :
        """ Swap englanted state """
        self.state = self.state[::-1]


    def measure(self, n) :
        """ Make a quantum measure on the n qubit.
            - n : qubit's name
        """
        # Check arguments type
        if not n in self.names.keys() :
            return "Qubit's name not found"
        
        # Compute proba for all states
        proba = np.array([], dtype=float)
        for i in self.state :
            proba = np.append(proba, (i*i.conjugate()).real)
    
        # Take a random number on [0, 1)
        random = np.random.ranf()

        # Method to select the global state
        #   |0>      |1>      |2>            |3>
        # |        |     |                |       |
        # |        |     |                |       |
        # 0--------p1--p1+p2--------p(i-1)+p(i)---1
        #                    ^
        #                  random
        #
        # In this exemple the selected global state will be |2>

        high_interv = 0
        for i in range(0, len(proba)) :
            low_interv = high_interv
            high_interv = high_interv + proba[i]
            
            # Check if the random number is in this interval
            if low_interv <= random and random < high_interv :

                # Force state after measure
                self.state = np.zeros(len(self.state), dtype=complex)
                self.state[i] = 1

                binary = self.dec2bin(i)

                # Return qubit value and global computer state projected
                return binary[ self.names[n] ], i
                



    def addFunction(self,name, f) :
        """ Record function for Uf and oracle gates.
            function : string contains an function
            ex :    f : 'y = cos(x) + 5 - exp(x) %2'
                    f : 'if x == 0 :
                            y = 1
                         else :
                            y = 0'
        """

        # To perform f function, we write function in a tempory file.
        # this file is an template python programm,
        # so we have just to import file with the function to perform it

        # Build programm
        program = "def {}(x) : \n".format(name)
        program += "    " # add tab
        for char in f :
            program += char
            if char == '\n' :
                program += "    " # add tab
        program += "\n    return y \n \n" # add return statement

        # Write programm
        temp = open('function.py', 'a')
        temp.write(program)
        temp.close()

        self.functions.append(name)

        # re-import function.py
        importlib.reload(function)


    def evalFunction(self, name, x) :
        """ Evaluate function recorded in functions.py """
        if not name in self.functions :
            return "Function not found"
        return getattr(function, name)(x)

    def dec2bin(self, i) :
        """ Swap a decimal nomber into bits split in a tab
            MSB at left
        """
        binary = []
        q = 1
        r = 0
        while q != 0 :
            q = int(i/2)
            r = i % 2
            binary.append(r)
            i = q

        # Full tab with 0
        while len(binary) < self.nbrQubit :
            binary.append(0)

        return binary[::-1] # reverse tab

    def bin2dec(self, binary) :
        """ Swap bits split in tab into decimal number.
            MSB at left"""

        # reverse tab
        binary = binary[::-1]
        dec = 0
        power = 0
        for b in binary :
            dec += b*2**power
            power += 1
        return dec


#####################################################################################################
###################################### End Simulator program ########################################
#####################################################################################################

class Parser :
    """ Represents the parser to use the current micro language's program """

    def __init__(self, language='en') :
        self.c = Computer()

        # function edition variable
        self.function_edit = False
        self.function = ""
        self.function_name = ''

        # repeat edition variable
        self.repeat_edition = False
        self.repeat_code = []
        self.repeat_iteration = 0

        if language == 'fr' :
            self.printHelp = self.printHelp_fr
        else :
            self.printHelp = self.printHelp_en

    def parse(self, ans) :
        """ Translate a string sequence into a computer command """
        query = ans.split(' ')
        
        # If the line is empty, parser don't care about this line
        if ans == "" :
            return None

        # If prompter is in function edition
        elif self.function_edit :
            if query[0]  == "endfunction" :
                self.c.addFunction(self.function_name, self.function)
                self.function_edit = False
            else :
                self.function += ans
                self.function += '\n'

            return None

        # If prompter is in repeat edition
        elif self.repeat_edition :
            if query[0] == "endrepeat" :
                self.repeat_edition = False

                # Do iteration
                result = ""
                for i in range(0, self.repeat_iteration) :
                    for line in self.repeat_code :
                        tmp= self.parse(line)
                        if tmp != None :   
                            result += tmp
                            result += '\n'
                return result
            else :
                self.repeat_code.append(ans)

            return None

        elif query[0] == "help" :
            if len(query) == 1:
                return self.printHelp()
            else :
                return self.printHelp(query[1])


        elif query[0] == "qubit" :
            if len(query) == 4 :
                try :
                    return self.c.addQubit(query[1], [float(query[2]), float(query[3])])
                except ValueError :
                    return "Qubit's state must be number"
            else :
                return "'qubit' needs 3 options"


        elif query[0] == "clear" :
            return self.c.clear()


        elif query[0] == "function" :
            if len(query) == 2 :
                self.function_edit = True
                self.function = ""
                self.function_name = query[1]
            else :
                return "'function' needs 1 option"


        elif query[0] == "evaluate" :
            if len(query) == 3 :
                try :
                    return self.c.evalFunction(query[1], float(query[2]))
                except ValueError :
                    return "variable evaluated must be a number"
            else :
                return "'evaluate' needs 2 options"


        elif query[0] == "hadamard" :
            if len(query) > 1 :
                if len(query[1]) == 1 :
                    return self.c.hadamard(query[1])
                else :
                    tab = self.str2tab(query[1])
                    if tab is None :
                        return "Syntax error on the table.\nDONT use whitespace in table."
                    else :
                        return self.c.hadamard(tab)
            else : 
                return "'hadamard' needs at least 1 option"


        elif query[0] == "pi8" :
            if len(query) > 1 :
                if len(query[1]) == 1 :
                    return self.c.pi8(query[1])
                else :
                    tab = self.str2tab(query[1])
                    if tab is None :
                        return "Syntax error on the table.\nDONT use whitespace in table."
                    else :
                        return self.c.pi8(tab)
            else :
                return "'pi8' needs at least 1 option"

        elif query[0] == "R" :
            if len(query) > 2 :
                if len(query[2]) == 1 :
                    try :
                        return self.c.R(query[2], float(query[1]))
                    except ValueError : 
                        return "First option must be a number"
                else :
                    tab = self.str2tab(query[2])
                    if tab is None :
                        return "Syntax error on the table.\nDONT use whitespace in table."
                    else :
                        return self.c.R(tab)
            else :  
                return "R needs at least 2 options"

        elif query[0] == "X" :
            if len(query) > 1 :
                if len(query[1]) == 1 :
                    return self.c.X(query[1])
                else :
                    tab = self.str2tab(query[1])
                    if tab is None :
                        return "Syntax error on the table.\nDONT use whitespace in table."
                    else :
                        return self.c.X(tab)
            else :  
                return "X needs at least 1 option"

        elif query[0] == "Y" :
            if len(query) > 1 :
                if len(query[1]) == 1 :
                    return self.c.Y(query[1])
                else :
                    tab = self.str2tab(query[1])
                    if tab is None :
                        return "Syntax error on the table.\nDONT use whitespace in table."
                    else :
                        return self.c.Y(tab)
            else : 
                return "Y needs at least 1 option"

        elif query[0] == "Z" :
            if len(query) > 1 :
                if len(query[1]) == 1 :
                    return self.c.Z(query[1])
                else :
                    tab = self.str2tab(query[1])
                    if tab is None :
                        return "Syntax error on the table.\nDONT use whitespace in table."
                    else :
                        return self.c.Z(tab)
            else : 
                return "Z needs at least 1 option"

        elif query[0] == "cnot" :
            if len(query) > 2 :
                if len(query[1]) == 1 and len(query[2]) == 1 :
                    return self.c.cnot(query[1], query[2])
                else :
                    tab1 = self.str2tab(query[1])
                    tab2 = self.str2tab(query[2])
                    if tab1 is None or tab2 is None :
                        return "Syntax error in at least on table.\n DONT use whitespace in table."
                    else :
                        return self.c.cnot(tab1, tab2)
            else :
                return "'cnot' need at least 2 options"


        elif query[0] == "uf" :
            if len(query) > 3 :
                if len(query[1]) == 1 and len(query[2]) == 1 :
                    return self.c.uf(query[2], query[3], query[1])
                else :
                    tab1 = self.str2tab(query[2])
                    tab2 = self.str2tab(query[3])
                    if tab1 is None or tab2 is None :
                        return "Syntax error in at least on table.\n DONT use whitespace in table."
                    else :
                        return self.c.uf(tab1, tab2, query[1])
            else : 
                return "'uf' need at least 3  options"


        elif query[0] == "s0" :
            if len(query) > 1 :
                tab = self.str2tab(query[1])
                if tab is None :
                    return "Syntax erro in table.\n DONNT use whitespace in table."
                else :
                    return self.c.S0(tab)

        elif query[0] == "oracle" :
            if len(query) > 3 :
                tab = self.str2tab(query[2])
                if tab is None :
                    return "Syntax erro in table.\n DONNT use whitespace in table."
                else :
                    return self.c.oracle(tab, query[3], query[1])
            else :
                return "'oracle' takes 3 options"

        elif query[0] == "swap" :
            return self.c.swap()


        elif query[0] == "print" :
            return self.c.__str__()

        elif query[0] == "measure" :
            if len(query) == 2 :
                result = self.c.measure(query[1])
                return "qubit {} = {} (computer in state {})".format(query[1], result[0], result[1])
            else :
                return "'measure' need 1 option"

        elif query[0] == "repeat" :
            if len(query) == 2 :
                self.repeat_edition = True
                self.repeat_code = []
                try :
                    self.repeat_iteration = int(query[1])
                except ValueError :
                    return "Iteration must be an integer"
            else :
                return "'repeat need 1 option"


        elif query[0] == "exit" :
            pass

        # If the line begins by '#', parser don't care about this line
        elif query[0][0] == '#' :
            pass


        else :
            return "Invalid command : {}".format(query[0])

    def str2tab(self, string) :
        """Swap a string such as '[x y z ...]' into ['x', 'y', 'z', ...]"""

        # Check if the string is surrended by '[...]' and remove them
        if string[0] == '[' and string[-1] == ']' :
            # Remove first and last char and split by ','
            return string[1:-1].split(',')
        else :
            return None

    def printHelp_fr(self, query=None) :
        
        if query is None :
            ans  = "Le simulateur dispose des commandes suivantes : \n"
            ans += "    qubit       : ajoute une qubit à l'ordinateur \n"
            ans += "    clear       : supprime le cache des noms de qubits et remet l'état de l'ordinateur à zero\n"
            ans += "    function    : ajoute une fonction à l'ordinateur qui pourra être utilisée dans les portes Uf et oracle\n"
            ans += "    evaluate    : évalue une fonction au nombre souhaité. (Permet de vérifer l'ajout d'une fonction) \n"
            ans += "    hadamard    : applique la porte de hadamard sur le qubit voulu \n"
            ans += "    pi8         : applique la porte T ou S(Pi/8) sur le qubit voulu \n"
            ans += "    R           : applique la porte R [exp(i*a) 0; 0 exp(-ia)] sur le qubit voulu\n"
            ans += "    X           : applique la matrice de pauli X sur le qubit voulu\n"
            ans += "    Y           : applique la matrice de pauli Y sur le qubit voulu\n"
            ans += "    Z           : applique la matrice de pauli Z sur le qubit voulu\n"
            ans += "    cnot        : applique la porte C-NOT sur les deux qubits voulus \n"
            ans += "    uf          : applique la porte Unitaire f(x) sur les deux qubits voulus avec la fonction voulue \n"
            ans += "    oracle      : applique l'oracle sur le registre de qubit et le qubit resultat voulus avec la fonction voulue \n"
            ans += "    s0          : applique la porte Shift zero (multiplie par -1 tous les états sauf |0> )\n"
            ans += "    swap        : inverse l'ordre des coefficients dans l'état intriqué\n"
            ans += "    print       : affiche l'état actuel de l'ordinateur (commande irréaliste, juste utile au debug)\n"
            ans += "    measure     : mesure d'une manière quantique (avec probabilité et dégénérescence) le qubit voulu \n"
            ans += "    repeat      : répéte un bloc de code plusieurs fois\n"
            ans += "    exit        : quitte simulateur \n \n"
            ans += "Pour plus d'information sur une de ces commandes, taper : 'help <command>' "

            return ans
        
        elif query == "qubit" :
            ans  = "Ajoute une qubit à l'ordinateur. Si l'ordinateur possède déjà un qubit, alors il devient intriqué \n \n"
            ans += "    >>> qubit <name> <a> <b> \n \n"
            ans += " <name>     : nom du qubit. ex : 'x', 'a', 'q1' \n"
            ans += " <a> et <b> : état du qubit tel que a*|0> + b*|1> avec a^2 + b^2 = 1 \n"

            return ans

        elif query == "clear" :
            return "Supprime le cache des noms de qubits et remet l'état de l'ordinateur à zero\n"
            

        elif query == "function" :
            ans  = "Ajoute une fonction à l'ordinateur qui pourra être utilisée dans les portes Uf et oracle\n"
            ans += "Toute la syntaxe python est disponible. i.e. if, else, for, ... \n"
            ans += "Les fonctions usuelles sont également disponible. i.e cos, sin, abs, sqtr, ... \n \n"
            ans += "    >>> function <name>\n"
            ans += "    function edition > <function> \n"
            ans += "    function edition > endfunction \n \n"
            ans += " <name>     : nom de la fonction. ex. 'f', 'g', 'f1', etc\n"
            ans += " <function> : code en syntaxe python évaluant une varibale y\n"
            ans += "\nExemples de fonctions valides : \n"
            ans += "    >>> function f\n"
            ans += "    function edition > y = x \n"
            ans += "    function efition > endfunction \n \n"
            ans += "    >>> function h\n"
            ans += "    function edition > y = 1 \n"
            ans += "    function edition > endfunction \n \n"
            ans += "    >>> function t\n"
            ans += "    function edition > if x == 0 :\n"
            ans += "    function edition >     y = 1 \n"
            ans += "    function edition > if x ==1 :\n"
            ans += "    function edition >     y = (cos(pi/4) + 1j*sin(pi/4))/sqrt(2)\n"
            ans += "    function edition > endfunction \n \n"
            
            return ans

        elif query == "evaluate" :
            ans  = "Evalue une fonction au nombre souhaité. (Permet de vérifer l'ajout d'une fonction) \n \n"
            ans += "    >>> evaluate <name> <x>\n \n"
            ans += "<name>  : nom de la fonction à évaluer.\n"
            ans += "<x>     : valeur\n"

            return ans

        elif query == "hadamard" :
            ans  = "Applique la porte de hadamard sur le qubit voulu \n \n"
            ans += "    >>> hadamard <name> \n \n"
            ans += "<name>  : nom du qubit ou tableau de noms sous la forme [x,y,z,...]"

            return ans

        elif query == "pi8" :
            ans  = "Applique la porte S(pi/8) sur le qubit voulu \n \n"
            ans += "    >>> pi8 <name> \n \n"
            ans += "<name>  : nom du qubit ou tableau de noms sous la forme [x,y,z,...]"

            return ans
        
        elif query == "R" :
            ans  = "Applique la porte R [exp(i*a) 0; 0 exp(-i*a)] sur le qubit voulu \n \n"
            ans += "    >>> R a <name> \n \n"
            ans += "a       : réel à utiliser dans l'exponentielle\n"
            ans += "<name>  : nom du qubit ou tableau de noms sous la forme [x,y,z,...]"

            return ans

        elif query == "X" :
            ans  = "Applique la matrice de pauli X sur le qubit voulu \n \n"
            ans += "    >>> X <name> \n \n"
            ans += "<name>  : nom du qubit ou tableau de noms sous la forme [x,y,z,...]"

            return ans

        elif query == "Y" :
            ans  = "Applique la matrice de pauli Y sur le qubit voulu \n \n"
            ans += "    >>> Y <name> \n \n"
            ans += "<name>  : nom du qubit ou tableau de noms sous la forme [x,y,z,...]"

            return ans

        elif query == "Z" :
            ans  = "Applique la matrice de pauli Z sur le qubit voulu \n \n"
            ans += "    >>> Z <name> \n \n"
            ans += "<name>  : nom du qubit ou tableau de noms sous la forme [x,y,z,...]"

            return ans

        elif query == "cnot" :
            ans  = "Applique la porte C-NOT sur les qubits voulus \n \n"
            ans += "    >>> cnot <name1> <name2>\n \n"
            ans += "<name1> : nom du premier qubit ou tableau de noms sous la forme [x,y,z,...]\n"
            ans += "<name2> : nom du second qubit ou tableau de noms sous la forme [x,y,z,...]"

            return ans

        elif query == "uf" :
            ans  = "Applique la porte Uf sur les qubits voulus avec la fonction voulus\n \n"
            ans += "    >>> cnot <f> <name1> <name2>\n \n"
            ans += "<f>     : nom de la fonction enregistrée par la commande 'function'\n"
            ans += "<name1> : nom du premier qubit ou tableau de noms sous la forme [x,y,z,...]\n"
            ans += "<name2> : nom du second qubit ou tableau de noms sous la forme [x,y,z,...]"

            return ans

        elif query == "oracle" :
            ans  = "Applique l'oracle sur le registre de qubit et le qubit resultat voulus avec la fonction voulue \n \n"
            ans += "    >>> oracle <f> <name> <q>\n\n"
            ans += "<f>     : nom de la fonction enregistrée par la commande 'function'\n"
            ans += "<name>  : tableau de noms sous la forme [x,y,z,...]\n"
            ans += "<q>     : nom du qubit resultat\n"

            return ans
        elif query == "s0" :
            ans  = "Applique la porte S0 (multiplie par -1 tous les états sauf |0> )\n\n"
            ans += "    >>> s0 <name>\n\n"
            ans += "<name>  : tableau de noms sous la forme [x,y,z,...]\n"

            return ans

        elif query == "swap" :
            ans  = "Renverse l'ordre des coéfficients dans l'état intriqué\n\n"
            ans += "    >>> swap\n\n"
            
            return ans

        elif query == "print" :
            return "Affiche l'état actuel de l'ordinateur (commande irréaliste, juste utile au debug"

        elif query == "measure" :
            ans  = "Mesure d'une manière quantique (avec probabilité et dégénérésence) le qubit voulu \n \n"
            ans += "    >>> measure <name> \n \n"
            ans += "<name>  : nom du qubit à mesurer"

            return ans

        elif query == "repeat" :
            ans  = "Répéte une bloc de code plusieurs fois\n\n"
            ans += "    >>> repeat <i>\n"
            ans += "    repeat edition > <code>\n"
            ans += "    repeat edition > <code>\n"
            ans += "    ...\n"
            ans += "    repeat edition > endrepeat\n\n"
            ans += "<i>     : nombre de fois à répéter\n"
            ans += "<code>  : bloc de code\n"

            return ans


    def printHelp_en(self, query=None) :
        
        if query is None :
            ans  = "Functions available in the simulator : \n"
            ans += "    qubit       : add a qubit in the computer \n"
            ans += "    clear       : delete the buffer qubit's names and set at zero the global state\n"
            ans += "    function    : add a function in the computer which can be used by Uf and oracle gates\n"
            ans += "    evaluate    : eval a function at a specific number \n"
            ans += "    hadamard    : perform hadamard gate \n"
            ans += "    pi8         : perform T or S(Pi/8) gate\n"
            ans += "    R           : perfrom R [exp(i*a) 0; 0 exp(-i*a)] gate\n"
            ans += "    X           : perfrom X pauli matrix\n"
            ans += "    Y           : perfrom Y pauli matrix\n"
            ans += "    Z           : perfrom Z pauli matrix\n"
            ans += "    cnot        : perform C-NOT gate\n"
            ans += "    uf          : perform Uf gate with available function\n"
            ans += "    oracle      : perform oracle gate with available function\n"
            ans += "    s0          : perfrom Shift phase zero gate \n"
            ans += "    swap        : swap the englanted state\n"
            ans += "    print       : print global state (not realistic function. Just to debug program)\n"
            ans += "    measure     : measure state by quantum approch (probabilistic and degeneracy)\n"
            ans += "    repeat      : repeat a bloc code many time\n"
            ans += "    exit        : quit simulator \n \n"
            ans += "For more information about one command, type : 'help <command>' "

            return ans
        
        elif query == "qubit" :
            ans  = "Add a qubit in the computer. If computer has another qubit, it becames englanted \n \n"
            ans += "    >>> qubit <name> <a> <b> \n \n"
            ans += " <name>     : qubit's name. ex : 'x', 'a', 'q1' \n"
            ans += " <a> et <b> : qubit's state such as a*|0> + b*|1> avec a^2 + b^2 = 1 \n"

            return ans

        elif query == "clear" :
            return "Delete the buffer qubi's names and set at zero the global state\n"
            

        elif query == "function" :
            ans  = "Add a function in the computer which can be used by Uf and oracle gates\n"
            ans += "All syntax python are availabe. i.e. if, else, for, ... \n"
            ans += "Main function are availabe too. i.e cos, sin, abs, sqtr, ... \n \n"
            ans += "    >>> function <name>\n"
            ans += "    function edition > <function> \n"
            ans += "    function edition > endfunction \n \n"
            ans += " <name>     : function's name. ex. 'f', 'g', 'f1', etc\n"
            ans += " <function> : python code evualating a y variable\n"
            ans += "\nSome examples of correct functions : \n"
            ans += "    >>> function f\n"
            ans += "    function edition > y = x \n"
            ans += "    function efition > endfunction \n \n"
            ans += "    >>> function h\n"
            ans += "    function edition > y = 1 \n"
            ans += "    function edition > endfunction \n \n"
            ans += "    >>> function t\n"
            ans += "    function edition > if x == 0 :\n"
            ans += "    function edition >     y = 1 \n"
            ans += "    function edition > if x ==1 :\n"
            ans += "    function edition >     y = (cos(pi/4) + 1j*sin(pi/4))/sqrt(2)\n"
            ans += "    function edition > endfunction \n \n"
            
            return ans

        elif query == "evaluate" :
            ans  = "Evaluate function at specific number \n \n"
            ans += "    >>> evaluate <name> <x>\n \n"
            ans += "<name>  : function's name.\n"
            ans += "<x>     : number\n"

            return ans

        elif query == "hadamard" :
            ans  = "Perform hadamard gate\n \n"
            ans += "    >>> hadamard <name> \n \n"
            ans += "<name>  : qubit's name or tab qubit's names [x,y,z,...]"

            return ans

        elif query == "pi8" :
            ans  = "Perform S(Pi/8) gate\n \n"
            ans += "    >>> pi8 <name> \n \n"
            ans += "<name>  : qubit's name or tab qubit's names [x,y,z,...]"

            return ans

        elif query == "R" :
            ans  = "Perform R [exp(i*a) 0; 0 exp(-i*a)] gate\n \n"
            ans += "    >>> R a <name> \n \n"
            ans += "a       : float to be use in the exponentiel\n"
            ans += "<name>  : qubit's name or tab qubit's names [x,y,z,...]"

            return ans
        
        elif query == "X" :
            ans  = "Perfrom X matrix Pauli\n \n"
            ans += "    >>> X <name> \n \n"
            ans += "<name>  : qubit's name or tab qubit's names [x,y,z,...]"

            return ans

        elif query == "Y" :
            ans  = "Perfrom Y matrix Pauli\n \n"
            ans += "    >>> Y <name> \n \n"
            ans += "<name>  : qubit's name or tab qubit's names [x,y,z,...]"

            return ans

        elif query == "Z" :
            ans  = "Perfrom Z matrix Pauli\n \n"
            ans += "    >>> Z <name> \n \n"
            ans += "<name>  : qubit's name or tab qubit's names [x,y,z,...]"

            return ans

        elif query == "cnot" :
            ans  = "Perform C-NOT gate\n \n"
            ans += "    >>> cnot <name1> <name2>\n \n"
            ans += "<name1>  : qubit's name or tab qubit's names [x,y,z,...]"
            ans += "<name2>  : qubit's name or tab qubit's names [x,y,z,...]"

            return ans

        elif query == "uf" :
            ans  = "Perfrom Uf gate with available function\n \n"
            ans += "    >>> cnot <f> <name1> <name2>\n \n"
            ans += "<f>      : function's name recorded by 'function' command\n"
            ans += "<name1>  : qubit's name or tab qubit's names [x,y,z,...]"
            ans += "<name2>  : qubit's name or tab qubit's names [x,y,z,...]"

            return ans

        elif query == "oracle" :
            ans  = "Perform oracle gate with available function \n \n"
            ans += "    >>> oracle <f> <name> <q>\n\n"
            ans += "<f>     : function's name recorded by 'function' command\n"
            ans += "<name>  : tab qubit's names [x,y,z,...]"
            ans += "<q>     : result qubit's name\n"

            return ans
        elif query == "s0" :
            ans  = "Perform Shift phase zero gate\n\n"
            ans += "    >>> s0 <name>\n\n"
            ans += "<name> : tab qubit's names [x,y,z,...]"

            return ans

        elif query == "swap" :
            ans  = "Swap englanted state\n\n"
            ans += "    >>> swap\n\n"

            return ans

        elif query == "print" :
            return "Print global state (not realistic function. Just to debug program)\n"

        elif query == "measure" :
            ans  = "Measure state by quantum approch (probabilistic and degeneracy)\n"
            ans += "    >>> measure <name> \n \n"
            ans += "<name>  : qubit's name to measure"

            return ans

        elif query == "repeat" :
            ans  = "Repeat any code many time\n\n"
            ans += "    >>> repeat <i>\n"
            ans += "    repeat edition > <code>\n"
            ans += "    repeat edition > <code>\n"
            ans += "    ...\n"
            ans += "    repeat edition > endrepeat\n\n"
            ans += "<i>     : number of iteration\n"
            ans += "<code>  : any code line\n"

            return ans

#####################################################################################################
###############################  End Parser and Prompter program  ###################################
#####################################################################################################

if __name__ == "__main__" :
    import sys

    
    # if an argument has been given then parse reads the program 
    if len(sys.argv) > 1 :
        parser = Parser()
        
        program = open(sys.argv[1], 'r')
        lines = program.read()
        program.close()
        lines = lines.split('\n')
        # execute program line by line
        for line in lines :
            res = parser.parse(line)
            if not res is None :
                print(res)

    # Else start prompter
    else :
        ans = input("English or french ? [en/fr] >>> ")
        parser = Parser(ans)
        if ans == 'fr' :
            print("Bienvenue dans le similateur d'ordinateur quantique")
            print("Si vous souhaitez de l'aide, lancer la commande 'help'")
            print("Pour une aide sur une commande taper 'help' <command> \n \n")
        else :
            print("Welcome in the quantum computer simulator")
            print("type 'help' to read available commands")
            print("For more information about command, type 'help' <command> \n \n")

    
        ans = ""
        while ans != "exit" :
            if parser.function_edit :
                ans = input("function edition > ")
            elif parser.repeat_edition :
                ans = input("repeat edition > ")
            else :
                ans = input(">>> ")
            res = parser.parse(ans)
            if not res is None :
                print(res)



